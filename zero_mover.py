def move_zeros(array):
    non_zeros = list(filter(lambda x: isinstance(x, bool) or x != 0, array))
    zeros = list(filter(lambda x: not isinstance(x, bool) and x == 0, array))
    return non_zeros + zeros
